# Spring Boot Coding Exercise

Test link - https://bitbucket.org/c967784/spring-boot-coding-exercise/src/master/

Completed the second task for the given exercise

# Task

Find the oldest user accounts with zero followers

Use the GitHub API to expose an endpoint in this microservice that will find the oldest accounts with zero followers.

The endpoint should accept a parameter that sets the number of accounts to return.

The following fields should be returned:

  1)id  2)login 3)html_url
  
# Solution

- Added new package to microservice module i.e - com.telstra.codechallenge.github
- Containing Model, Service and Controller to segerate the code for given task
- Exposed new endpoint "/users" to get the oldest users having number of followers using GitHub API
- The API can accept two parameters as "followers and count" to fetch the users
	- "followers" - Accounts having number of followers
	- "count" - The number of accounts to be returned
- These parameters are optional and default values are 0 and 3 for followers and count respectivly.

# Example

**1. Users API Call

- Request: http://localhost:8080/users

- Response: [
    {
        "id": 13064110,
        "login": "mercuryphp",
        "html_url": "https://github.com/mercuryphp"
    },
    {
        "id": 13064136,
        "login": "devgarvit",
        "html_url": "https://github.com/devgarvit"
    },
    {
        "id": 13064166,
        "login": "FreeSpeak",
        "html_url": "https://github.com/FreeSpeak"
    }
]

**2. Users API call using parameters

- Request: http://localhost:8080/users?followers=0&count=1

- Response: [
    {
        "id": 13064110,
        "login": "mercuryphp",
        "html_url": "https://github.com/mercuryphp"
    }
]

  
