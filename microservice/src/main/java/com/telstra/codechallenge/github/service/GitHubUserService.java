package com.telstra.codechallenge.github.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.github.module.Item;
import com.telstra.codechallenge.github.module.UserResponse;

@Service
public class GitHubUserService {
	
	@Value("${github.base.url}")
	private String gitHubBaseUrl;
	
	@Autowired
	private RestTemplate restTemplate;
	
	public GitHubUserService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public List<Item> getItems(String followers, String count) {
		UserResponse response = restTemplate.
				getForObject(gitHubBaseUrl+"/search/users?q=followers:{followers}&order=asc&per_page={count}",
				UserResponse.class,followers,count);
		return response.getItems();
	}
}
