package com.telstra.codechallenge.github.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.github.module.Item;
import com.telstra.codechallenge.github.service.GitHubUserService;

@RestController
public class GitHubUserController {

	@Autowired
	GitHubUserService userService;
	
	public GitHubUserController(GitHubUserService userService) {
		this.userService = userService;
	}
	
	@RequestMapping(path = "/users", method = RequestMethod.GET, produces="application/json")
	  public List<Item> quotes(@RequestParam(value="followers", required=false, defaultValue = "0") String followers,
			    				@RequestParam(value="count", required=false, defaultValue = "3") String count) {
	    		return userService.getItems(followers,count);
	  }
}
